package org.softserve.opencartapp.tests.FTC.ContactUsPage;

import org.junit.Test;
import org.softserve.opencartapp.pages.ContactUsPage;
import org.softserve.opencartapp.core.BaseTest;
import org.softserve.opencartapp.utilities.ConstTextValues;

public class ContactUsPage_SubmittingForm_ScenarioNegative extends BaseTest {
    ContactUsPage page;

    @Test
    public void test(){
        page = new ContactUsPage(getDriver());
        page.goToPage();
        page.clickSubmitButton()
                .verifyNameFieldErrorShownAndCorrect(ConstTextValues.nameErrorMsg)
                .verifyEmailFieldErrorShownAndCorrect(ConstTextValues.eMailErrorMsg)
                .verifyEnquiryFieldErrorShownAndCorrect(ConstTextValues.enquiryErrorMsg)
                .fillYourNameFormName(ConstTextValues.name)
                .clickSubmitButton()
                .verifyEmailFieldErrorShownAndCorrect(ConstTextValues.eMailErrorMsg)
                .verifyEnquiryFieldErrorShownAndCorrect(ConstTextValues.enquiryErrorMsg)
                .fillEmailFormEmail(ConstTextValues.eMail)
                .clickSubmitButton()
                .verifyEnquiryFieldErrorShownAndCorrect(ConstTextValues.enquiryErrorMsg);

    }
}
