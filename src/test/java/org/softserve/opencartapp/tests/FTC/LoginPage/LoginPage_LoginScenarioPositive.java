package org.softserve.opencartapp.tests.FTC.LoginPage;

import org.junit.Test;
import org.softserve.opencartapp.core.BaseTest;
import org.softserve.opencartapp.pages.LoginPage;
import org.softserve.opencartapp.utilities.ConstTextValues;

public class LoginPage_LoginScenarioPositive extends BaseTest {

    LoginPage page;

    @Test
    public void test(){
        page = new LoginPage(getDriver());
        page.goToPage();
        page.fillEmailOnLoginPage(ConstTextValues.validEmailAddressForLogin)
                .fillPasswordOnLoginPage(ConstTextValues.passwordDeffault)
                .clickLoginButtonAndMoveToAccountPage()
                .verifyCurrentUrl(ConstTextValues.currentAccPageUrl);

    }

}
