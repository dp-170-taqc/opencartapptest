package org.softserve.opencartapp.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.softserve.opencartapp.core.BasePage;

public class SearchPage extends BasePage{

    public SearchPage(WebDriver driver){
        super(driver);
    }

    public void goToPage() {
        getDriver().get(getBaseUrl() + "index.php?route=product/search");
    }

    By addToCartBtn = By.xpath("//div[@class=\"button-group\"]/button/i[@class=\"fa fa-shopping-cart\"]");

    public SearchPage clickAddBtn(){
        getElement(addToCartBtn).click();
        return this;
    }
}
