package org.softserve.opencartapp.admin.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.softserve.opencartapp.core.BasePage;
import org.softserve.opencartapp.pages.HomePage;

public class AdminMainPage extends BasePage {

    By usernameField = By.xpath("//*[@id=\"input-username\"]");
    By passwordField = By.xpath("//*[@id=\"input-password\"]");
    By loginBtn = By.xpath("//button[@type='submit']");
    By shoppingCartMenu = By.cssSelector(".parent > .fa-shopping-cart");
    By ordersSubMenuOfShopCartMenu = By.cssSelector("#menu-sale > ul > li:nth-child(1) > a");
    By orderIdCheckBox = By.xpath("//input[@type='checkbox']");
    By deleteBtn = By.xpath("//button[@id='button-delete']");

    public AdminMainPage(WebDriver driver) {
        super(driver);
    }

    public void goToPage() {
        getDriver().get(getBaseUrl() + "admin");
    }

    public AdminMainPage createNewInstanceHomePage() {
        return new AdminMainPage(getDriver());
    }

    public AdminMainPage fillUsernameField(String s) {
        getElement(usernameField).sendKeys(s);
        return this;
    }

    public AdminMainPage fillPasswordField(String s) {
        getElement(passwordField).sendKeys(s);
        return this;
    }

    public AdminMainPage clickLoginBtn() {
        getElement(loginBtn).click();
        return this;
    }

    public AdminMainPage clickShoppingMenu() {
        getElement(shoppingCartMenu).click();
        return this;
    }

    public AdminMainPage clickOrdersSubMenu() {
        getElement(ordersSubMenuOfShopCartMenu).click();
        return this;
    }

    public AdminMainPage selectOdredId() {
        getElement(orderIdCheckBox).click();
        return this;
    }

    public AdminMainPage clickDeleteBtn() {
        getElement(deleteBtn).click();
        return this;
    }
}
