package org.softserve.opencartapp.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.softserve.opencartapp.core.BasePage;

public class ShoppingCartListPage extends BasePage {

    private HomePage homePage = new HomePage(getDriver());
    private ProductPage productPage = new ProductPage(getDriver());

    public ShoppingCartListPage(WebDriver driver) {
        super(driver);
    }

    public void goToPage() {
        getDriver().get(getBaseUrl() + "index.php?route=checkout/cart");
    }

    By addToCartDropDown = By.xpath("//button[@data-toggle='dropdown']");
    By viewCartBtn = By.xpath("//*[@id=\"cart\"]/ul/li[2]/div/p/a[1]/strong");
    By shoppingCartLink = By.xpath("//*[@id=\"top-links\"]/ul/li[4]");
    By useCouponCode = By.xpath("//h4[contains(@class,\"panel\")]/a[contains(@href,\"coupon\")]");
    By estimateShippingTaxes = By.xpath("//*[@id=\"accordion\"]/div[2]/div[1]");
    By useGiftCertificate = By.xpath("//a[@href='#collapse-voucher']");
    By enterCouponField = By.xpath("//input[contains(@id,\"input-coupon\")]");
    By applyCouponBtn = By.id("button-coupon");
    By enterGiftCert = By.xpath("//input[contains(@id,\"input-voucher\")]");
    By applyGiftCertBtn = By.id("button-voucher");
    By warningmessagecoupon = By.xpath("/html/body/div[3]/div[1]");
    By successmessagecoupon = By.xpath("/html/body/div[3]/div[1]");
    By quantity = By.xpath("//input[contains(@name,\"quantity\")]");
    By refreshBtn = By.xpath("//*[@id=\"content\"]/form/div/table/tbody/tr/td[4]/div/span/button[1]");
    By checkoutBtn = By.xpath("//*[@class=\"pull-right\"]");
    By emptyShoppingCart = By.xpath("//ul[contains(@class,'dropdown-menu')]//li//p");
    By continueShoppingBtn = By.xpath("//a[contains(text(),'Continue Shopping')]");

    public ShoppingCartListPage setCouponCode(String couponCode) {
        getElement(enterCouponField).sendKeys(couponCode);
        return this;
    }

    public ShoppingCartListPage setGiftCertificate(String giftCertificate) {
        getElement(enterGiftCert).sendKeys(giftCertificate);
        return this;
    }

    public ShoppingCartListPage clickUseCouponCode() {
        getElement(useCouponCode).click();
        return new ShoppingCartListPage(getDriver());
    }

    public ShoppingCartListPage clickUseGiftCertificate() {
        wait(3000);
        getElement(useGiftCertificate).click();
        return new ShoppingCartListPage(getDriver());
    }

    public ShoppingCartListPage clickApplyCouponBtn(){
        wait(3000);
        getElement(applyCouponBtn).click();
        return new ShoppingCartListPage(getDriver());
    }

    public ShoppingCartListPage clickApplyGiftCertificate() {
        getElement(applyGiftCertBtn).click();
        return new ShoppingCartListPage(getDriver());
    }

    public ShoppingCartListPage setQuantity(String value) {
        getElement(quantity).sendKeys(value);
        return this;
    }

    public ShoppingCartListPage clickRefreshForQuantity() {
        getElement(refreshBtn).click();
        return new ShoppingCartListPage(getDriver());
    }

    public CheckoutPage clickCheckout() {
        wait(3000);
        getElement(checkoutBtn).click();
        return new CheckoutPage(getDriver());
    }

    public ShoppingCartListPage clearQuantityField() {
        getElement(quantity).clear();
        return this;
    }

    public ShoppingCartListPage clickAddToShoppingCart() {
        getElement(addToCartDropDown).click();
        return this;
    }

    public By getEmptyShoppingCart() {
        return emptyShoppingCart;
    }

    public HomePage clickContinueShoppingBtn() {
        getElement(continueShoppingBtn).click();
        return new HomePage(getDriver());
    }
}


