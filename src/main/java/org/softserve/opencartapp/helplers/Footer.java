package org.softserve.opencartapp.helplers;

import org.openqa.selenium.WebDriver;
import org.softserve.opencartapp.core.BasePage;

public class Footer extends BasePage {

    public Footer(WebDriver driver) {
        super(driver);
    }

    public void goToPage() {
        getDriver().get(getBaseUrl() + "index.php");
    }
}
