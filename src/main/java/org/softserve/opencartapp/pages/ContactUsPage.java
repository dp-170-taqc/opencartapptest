package org.softserve.opencartapp.pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.softserve.opencartapp.core.BaseElement;
import org.softserve.opencartapp.core.BasePage;


public class ContactUsPage extends BasePage {

    By contactUsTitle = By.xpath("//*[@id=\"content\"]/h1");
    By ourLocationInfoTitle = By.xpath("//*[@id=\"content\"]/h3");
    By ourLocationInfoInformation = By.xpath("//*[@id=\"content\"]/div");
    By contactFormBlockTitle = By.xpath("//*[@id=\"content\"]/form/fieldset/legend");
    By yourNameInputForm = By.xpath("//*[@id=\"input-name\"]");
    By eMailAdressForm = By.xpath("//*[@id=\"input-email\"]");
    By enquryForm = By.xpath("//*[@id=\"input-enquiry\"]");
    By submitButton = By.xpath("//*[@id=\"content\"]/form/div/div/input");
    By emailErrMsg = By.xpath("//*[@id=\"content\"]/form/fieldset/div[2]/div/div");
    By nameErrMsg = By.xpath("//*[@id=\"content\"]/form/fieldset/div[1]/div/div");
    By enquryErrMsg = By.cssSelector("#content > form > fieldset > div:nth-child(4) > div > div");
    String expectedTitle = "Contact Us";

    public ContactUsPage(WebDriver driver) {
        super(driver);
    }

    public void goToPage() {
        getDriver().get(getBaseUrl() + "index.php?route=information/contact");
    }


    public ContactUsPage checkedVisiblityContactUsTitle(){
        Assert.assertTrue(getElement(contactUsTitle).isExists());
        return this;
    }

    public ContactUsPage checkedVisiblitourLocationInfoTitle(){
        Assert.assertTrue(getElement(ourLocationInfoTitle).isExists());
        return this;
    }

    public ContactUsPage checkedVisiblitOurLocationInfoInformation(){
        Assert.assertTrue(getElement(ourLocationInfoInformation).isExists());
        return this;
    }
    public ContactUsPage checkedVisiblitContactFormBlockTitle(){
        Assert.assertTrue(getElement(contactFormBlockTitle).isExists());
        return this;
    }
    public ContactUsPage checkedVisiblitYourNameInputForm(){
        Assert.assertTrue(getElement(yourNameInputForm).isExists());
        return this;
    }
    public ContactUsPage checkedVisiblitEMailAdressForm(){
        Assert.assertTrue(getElement(eMailAdressForm).isExists());
        return this;
    }
    public ContactUsPage checkedVisiblityEnquryForm(){
        Assert.assertTrue(getElement(enquryForm).isExists());
        return this;
    }
    public ContactUsPage checkedVisiblitSubmitButton(){
        Assert.assertTrue(getElement(submitButton).isExists());
        return this;
    }

    public ContactUsPage verifyNameFieldErrorShownAndCorrect(String error){
        BaseElement nameError = getElement(nameErrMsg);
        Assert.assertTrue(nameError.isExists());
        Assert.assertEquals(nameError.getValue(), error);
        return this;
    }

    public ContactUsPage verifyEmailFieldErrorShownAndCorrect(String error){
        BaseElement eMailError = getElement(emailErrMsg);
        Assert.assertTrue(eMailError.isExists());
        Assert.assertEquals(eMailError.getValue(), error);
        return this;
    }

    public ContactUsPage verifyEnquiryFieldErrorShownAndCorrect(String error){
        BaseElement enquiryError = getElement(enquryErrMsg);
        Assert.assertTrue(enquiryError.isExists());
        Assert.assertEquals(enquiryError.getValue(), error);
        return this;
    }


    public ContactUsPage clickSubmitButton(){
        getElement(submitButton).click();
        return this;
    }

    public SuccessPage clickSubmitButtonAndMoveToSuccessPage(){
        getElement(submitButton).click();
        return new SuccessPage(getDriver());
    }


    public ContactUsPage fillYourNameFormName(String value){
        getElement(yourNameInputForm).sendKeys(value);
        return this;
    }
    public ContactUsPage fillEmailFormEmail(String value){
        getElement(eMailAdressForm).sendKeys(value);
        return this;
    }
    public ContactUsPage fillEnquiry(String value){
        getElement(enquryForm).sendKeys(value);
        return this;
    }

}


