package org.softserve.opencartapp.tests.STC.HomePage;

import org.junit.Test;
import org.softserve.opencartapp.core.BaseTest;
import org.softserve.opencartapp.helplers.Header;

public class HeaderPage_WholeVisibility extends BaseTest {

    private Header page;

    @Test
    public void checkedMainMenu()  {
        page = new Header(getDriver());
        page.goToPage();
        page.checkedVisiblityMainMenuTopBar()
                .checkedVisiblityMainMenuNavBar()
                .checkedVisiblityMainMenuBreadCrumb();
    }
}
