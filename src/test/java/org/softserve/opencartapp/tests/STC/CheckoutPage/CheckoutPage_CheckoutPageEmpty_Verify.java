package org.softserve.opencartapp.tests.STC.CheckoutPage;

import org.junit.Test;
import org.softserve.opencartapp.core.BaseTest;
import org.softserve.opencartapp.pages.CheckoutPage;
import org.softserve.opencartapp.utilities.ConstTextValues;

public class CheckoutPage_CheckoutPageEmpty_Verify extends BaseTest{

    private CheckoutPage page;

    @Test
    public void test(){
        page = new CheckoutPage(getDriver());
        page.goToPage();
        page.checkedVisiblityCheckoutPageTitle()
                .checkedVisiblityLabelIsEmpty(ConstTextValues.shoppingCartEmptyMsg);
    }
}
