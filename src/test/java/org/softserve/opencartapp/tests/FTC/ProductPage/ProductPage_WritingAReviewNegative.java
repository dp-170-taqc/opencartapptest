package org.softserve.opencartapp.tests.FTC.ProductPage;

import org.junit.Test;
import org.softserve.opencartapp.core.BaseTest;
import org.softserve.opencartapp.pages.HomePage;

public class ProductPage_WritingAReviewNegative extends BaseTest {

    @Test
    public void writingAReviewTestNegative() {
        HomePage homePage = new HomePage(getDriver());
        homePage.goToPage();
        homePage.goToIphonePageFromFeatured()
                .openReviewLink()
                .clickContinueBtn()
                .checkedErrorMsg();
    }
}
