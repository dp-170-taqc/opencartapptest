package org.softserve.opencartapp.tests.EndToEnd;

import org.junit.Test;
import org.softserve.opencartapp.core.BaseTest;
import org.softserve.opencartapp.pages.LoginPage;
import org.softserve.opencartapp.utilities.ConstTextValues;

public class ETE_RegisterLoginAndChangePassword extends BaseTest {

    LoginPage page;

    @Test
    public void test(){
        page = new LoginPage(getDriver());
        page.goToPage();
        page.fillEmailOnLoginPage(ConstTextValues.validEmailAddressForLogin)
                .fillPasswordOnLoginPage(ConstTextValues.passwordDeffault)
                .clickLoginButtonAndMoveToAccountPage()
                .verifyCurrentUrl(ConstTextValues.currentAccPageUrl)
                .clickChangeYourPasswordAndMoveToAccountChangePasswordPage()
                .fillFormToFillPassword(ConstTextValues.password1to5)
                .fillFormToFillConfirmPassword(ConstTextValues.password1to5)
                .clickContinueButton()
                .clickLogout()
                .clickLoginButtonFromRightBlock()
                .fillEmailOnLoginPage(ConstTextValues.validEmailAddressForLogin)
                .fillPasswordOnLoginPage(ConstTextValues.password1to5)
                .clickLoginButtonAndMoveToAccountPage()
                .verifyCurrentUrl(ConstTextValues.currentAccPageUrl);

    }

}
