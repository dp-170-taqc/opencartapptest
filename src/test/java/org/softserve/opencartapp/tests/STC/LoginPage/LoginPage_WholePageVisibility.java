package org.softserve.opencartapp.tests.STC.LoginPage;

import org.junit.Test;
import org.softserve.opencartapp.core.BaseTest;
import org.softserve.opencartapp.pages.ContactUsPage;
import org.softserve.opencartapp.pages.LoginPage;

public class LoginPage_WholePageVisibility extends BaseTest {
    LoginPage page;

    @Test
    public void test(){
        page = new LoginPage(getDriver());
        page.goToPage();
        page.checkedVisiblitForgottenPasswordLinkFromLinkBlockOnTheRight()
                .checkedVisiblityAddressBookLinkFromLinkBlockOnTheRight()
                .checkedVisiblityContinueButtonFromNewCustomerBlock()
                .checkedVisiblityDownloadsLinkFromLinkBlockOnTheRight()
                .checkedVisiblityEMailAddressForm2Fill()
                .checkedVisiblityForgottenPasswordLink()
                .checkedVisiblityLinkBlockOnTheRight()
                .checkedVisiblityLoginButtonFromReturningCustomerBlock()
                .checkedVisiblityLoginLinkFromLinkBlockOnTheRight()
                .checkedVisiblityMyAccountLinkFromLinkBlockOnTheRight()
                .checkedVisiblityNewCustomerBlock()
                .checkedVisiblityNewsletterLinkFromLinkBlockOnTheRight()
                .checkedVisiblityOrderHistoryLinkFromLinkBlockOnTheRight()
                .checkedVisiblityPasswordForm2Fill()
                .checkedVisiblityReccuringPaymentsLinkFromLinkBlockOnTheRight()
                .checkedVisiblityRegisterLinkFromLinkBlockOnTheRight()
                .checkedVisiblityReturnsLinkFromLinkBlockOnTheRight()
                .checkedVisiblityRewardPointsLinkFromLinkBlockOnTheRight()
                .checkedVisiblityTransactionLinkFromLinkBlockOnTheRight()
                .checkedVisiblityWishListLinkFromLinkBlockOnTheRight()
                .checkedVisiblityReturningCustomerBlock();
    }

}
