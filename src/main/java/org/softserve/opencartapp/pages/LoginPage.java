package org.softserve.opencartapp.pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.softserve.opencartapp.core.BaseElement;
import org.softserve.opencartapp.core.BasePage;

public class LoginPage extends BasePage {


    By newCustomerBlock = By.xpath("//*[@id=\"content\"]/div/div[1]/div");
    By continueButtonFromNewCustomerBlock = By.xpath("//*[@id=\"content\"]/div/div[1]/div/a");
    By returningCustomerBlock = By.xpath("//*[@id=\"content\"]/div/div[2]/div");
    By eMailAddressForm2Fill = By.xpath("//*[@id=\"input-email\"]");
    By passwordForm2Fill = By.xpath("//*[@id=\"input-password\"]");
    By forgottenPasswordLink = By.xpath("//*[@id=\"content\"]/div/div[2]/div/form/div[2]/a");
    By loginButtonFromReturningCustomerBlock = By.xpath("//*[@id=\"content\"]/div/div[2]/div/form/input[1]");
    By linkBlockOnTheRight = By.xpath("//*[@id=\"column-right\"]/div");
    By loginLinkFromLinkBlockOnTheRight = By.xpath("//*[@id=\"column-right\"]/div/a[1]");
    By registerLinkFromLinkBlockOnTheRight = By.xpath("//*[@id=\"column-right\"]/div/a[2]");
    By forgottenPasswordLinkFromLinkBlockOnTheRight = By.xpath("//*[@id=\"column-right\"]/div/a[3]");
    By myAccountLinkFromLinkBlockOnTheRight = By.xpath("//*[@id=\"column-right\"]/div/a[4]");
    By addressBookLinkFromLinkBlockOnTheRight = By.xpath("//*[@id=\"column-right\"]/div/a[5]");
    By wishListLinkFromLinkBlockOnTheRight = By.xpath("//*[@id=\"column-right\"]/div/a[6]");
    By orderHistoryLinkFromLinkBlockOnTheRight = By.xpath("//*[@id=\"column-right\"]/div/a[7]");
    By downloadsLinkFromLinkBlockOnTheRight = By.xpath("//*[@id=\"column-right\"]/div/a[8]");
    By reccuringPaymentsLinkFromLinkBlockOnTheRight = By.xpath("//*[@id=\"column-right\"]/div/a[9]");
    By rewardPointsLinkFromLinkBlockOnTheRight = By.xpath("//*[@id=\"column-right\"]/div/a[10]");
    By returnsLinkFromLinkBlockOnTheRight = By.xpath("//*[@id=\"column-right\"]/div/a[11]");
    By transactionLinkFromLinkBlockOnTheRight = By.xpath("//*[@id=\"column-right\"]/div/a[12]");
    By newsletterLinkFromLinkBlockOnTheRight = By.xpath("//*[@id=\"column-right\"]/div/a[13]");
    By loginErrorMsg = By.xpath("/html/body/div[3]/div[1]");

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public void goToPage() {
        getDriver().get(getBaseUrl() + "index.php?route=account/login");
    }

    public LoginPage checkedVisiblityNewCustomerBlock() {
        Assert.assertTrue(getElement(newCustomerBlock).isExists());
        return this;
    }

    public LoginPage checkedVisiblityContinueButtonFromNewCustomerBlock() {
        Assert.assertTrue(getElement(continueButtonFromNewCustomerBlock).isExists());
        return this;
    }

    public LoginPage checkedVisiblityReturningCustomerBlock() {
        Assert.assertTrue(getElement(returningCustomerBlock).isExists());
        return this;
    }

    public LoginPage checkedVisiblityEMailAddressForm2Fill() {
        Assert.assertTrue(getElement(eMailAddressForm2Fill).isExists());
        return this;
    }

    public LoginPage checkedVisiblityPasswordForm2Fill() {
        Assert.assertTrue(getElement(passwordForm2Fill).isExists());
        return this;
    }

    public LoginPage checkedVisiblityForgottenPasswordLink() {
        Assert.assertTrue(getElement(forgottenPasswordLink).isExists());
        return this;
    }

    public LoginPage checkedVisiblityLoginButtonFromReturningCustomerBlock() {
        Assert.assertTrue(getElement(loginButtonFromReturningCustomerBlock).isExists());
        return this;
    }

    public LoginPage checkedVisiblityLinkBlockOnTheRight() {
        Assert.assertTrue(getElement(linkBlockOnTheRight).isExists());
        return this;
    }

    public LoginPage checkedVisiblityLoginLinkFromLinkBlockOnTheRight() {
        Assert.assertTrue(getElement(loginLinkFromLinkBlockOnTheRight).isExists());
        return this;
    }

    public LoginPage checkedVisiblityRegisterLinkFromLinkBlockOnTheRight() {
        Assert.assertTrue(getElement(registerLinkFromLinkBlockOnTheRight).isExists());
        return this;
    }

    public LoginPage checkedVisiblitForgottenPasswordLinkFromLinkBlockOnTheRight() {
        Assert.assertTrue(getElement(forgottenPasswordLinkFromLinkBlockOnTheRight).isExists());
        return this;
    }

    public LoginPage checkedVisiblityMyAccountLinkFromLinkBlockOnTheRight() {
        Assert.assertTrue(getElement(myAccountLinkFromLinkBlockOnTheRight).isExists());
        return this;
    }

    public LoginPage checkedVisiblityAddressBookLinkFromLinkBlockOnTheRight() {
        Assert.assertTrue(getElement(addressBookLinkFromLinkBlockOnTheRight).isExists());
        return this;
    }

    public LoginPage checkedVisiblityWishListLinkFromLinkBlockOnTheRight() {
        Assert.assertTrue(getElement(wishListLinkFromLinkBlockOnTheRight).isExists());
        return this;
    }

    public LoginPage checkedVisiblityOrderHistoryLinkFromLinkBlockOnTheRight() {
        Assert.assertTrue(getElement(orderHistoryLinkFromLinkBlockOnTheRight).isExists());
        return this;
    }

    public LoginPage checkedVisiblityDownloadsLinkFromLinkBlockOnTheRight() {
        Assert.assertTrue(getElement(downloadsLinkFromLinkBlockOnTheRight).isExists());
        return this;
    }

    public LoginPage checkedVisiblityReccuringPaymentsLinkFromLinkBlockOnTheRight() {
        Assert.assertTrue(getElement(reccuringPaymentsLinkFromLinkBlockOnTheRight).isExists());
        return this;
    }

    public LoginPage checkedVisiblityRewardPointsLinkFromLinkBlockOnTheRight() {
        Assert.assertTrue(getElement(rewardPointsLinkFromLinkBlockOnTheRight).isExists());
        return this;
    }

    public LoginPage checkedVisiblityReturnsLinkFromLinkBlockOnTheRight() {
        Assert.assertTrue(getElement(returnsLinkFromLinkBlockOnTheRight).isExists());
        return this;
    }

    public LoginPage checkedVisiblityTransactionLinkFromLinkBlockOnTheRight() {
        Assert.assertTrue(getElement(transactionLinkFromLinkBlockOnTheRight).isExists());
        return this;
    }

    public LoginPage checkedVisiblityNewsletterLinkFromLinkBlockOnTheRight() {
        Assert.assertTrue(getElement(newsletterLinkFromLinkBlockOnTheRight).isExists());
        return this;
    }


    public LoginPage clickLoginButton() {
        getElement(loginButtonFromReturningCustomerBlock).click();
        return this;
    }

    public AccountPage clickLoginButtonAndMoveToAccountPage() {
        getElement(loginButtonFromReturningCustomerBlock).click();
        return new AccountPage(getDriver());
    }

    public LoginPage clearEMailLoginForm() {
        getElement(eMailAddressForm2Fill).clear();
        return this;
    }

    public LoginPage clearPasswordLoginForm() {
        getElement(passwordForm2Fill).clear();
        return this;
    }


    public LoginPage fillEmailOnLoginPage(String eMail) {
        getElement(eMailAddressForm2Fill).sendKeys(eMail);
        return this;
    }

    public LoginPage fillPasswordOnLoginPage(String password) {
        getElement(passwordForm2Fill).sendKeys(password);
        return this;
    }

    public LoginPage verifyLoginErrorMsgShownAndCorrect(String errorMsg) {
        BaseElement error = getElement(loginErrorMsg);
        Assert.assertTrue(error.isExists());
        Assert.assertEquals(error.getValue(), errorMsg);
        return this;
    }


}
