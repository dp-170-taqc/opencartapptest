package org.softserve.opencartapp.tests.STC.HomePage;

import org.junit.Test;
import org.softserve.opencartapp.core.BaseTest;
import org.softserve.opencartapp.pages.HomePage;
import org.softserve.opencartapp.pages.ProductPage;

public class HomePageClickability extends BaseTest {
    HomePage homePage;

    @Test
    public void macBookSliderClickability() {
        HomePage homePage = new HomePage(getDriver());
        homePage.goToPage();
        homePage.goToMacBookSlider();
    }

    @Test
    public void iPhoneSliderClickability() {
        HomePage homePage = new HomePage(getDriver());
        homePage.goToPage();
        homePage.goToIphoneSlider();
    }

    @Test
    public void macBookFeaturedClickability() {
        HomePage homePage = new HomePage(getDriver());
        homePage.goToPage();
        homePage.goToMacBookPageFromFeatured();
    }

    @Test
    public void iPhoneFeaturedClickability() {
        HomePage homePage = new HomePage(getDriver());
        homePage.goToPage();
        homePage.goToIphonePageFromFeatured();
    }

    @Test
    public void appleCinemaFeaturedClickability() {
        HomePage homePage = new HomePage(getDriver());
        homePage.goToPage();
        homePage.goToAppleCinemaPageFromFeatured();
    }

    @Test
    public void canonEosFeaturedClickability() {
        HomePage homePage = new HomePage(getDriver());
        homePage.goToPage();
        homePage.goToCanonEosPageFromFeatured();
    }
}
