package org.softserve.opencartapp.tests.FTC.ProductPage;

import org.junit.Test;
import org.softserve.opencartapp.core.BaseTest;
import org.softserve.opencartapp.pages.HomePage;
import org.softserve.opencartapp.pages.ProductPage;

import java.util.concurrent.TimeUnit;

public class ProductPage_PhotoOpened extends BaseTest {

    @Test
    public void photoOpenedTest() {
        HomePage homePage = new HomePage(getDriver());
        homePage.goToPage();
        homePage.goToMacBookPageFromFeatured()
                .openProductPhoto();
    }
}
