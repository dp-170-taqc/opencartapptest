package org.softserve.opencartapp.pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.softserve.opencartapp.core.BaseElement;
import org.softserve.opencartapp.core.BasePage;

public class CheckoutPage extends BasePage {

    By checkoutPageTitle = By.xpath("//div[@id=\"content\"]/h1");
    By checkoutPageEmptyLabel = By.xpath("//div[@id = 'content']/p");
    By itemMenuComponent = By.xpath("//*[@class = 'dropdown-toggle' and contains(text(), 'Component')]");
    By itemSubMenuMonitors = By.xpath("//*[contains(text(), 'Monitors')]");

    By checkoutOptionsStep1 = By.xpath("//*[contains(text(),'Step 1: Checkout Options')]");
    By continueButton = By.xpath("//input[@type=\"button\"]");

    By guestCheckoutRadioBtn = By.xpath("(//input[@name='account'])[2]");
    By continueBtnStep1 = By.xpath("//*[@id=\"button-account\"]");
    By firstNameField = By.xpath("//*[@id=\"input-payment-firstname\"]");
    By lastNameField = By.xpath("//*[@id=\"input-payment-lastname\"]");
    By emailField = By.xpath("//*[@id=\"input-payment-email\"]");
    By telephoneField = By.xpath("//*[@id=\"input-payment-telephone\"]");
    By address1Field = By.xpath("//*[@id=\"input-payment-address-1\"]");
    By cityField = By.xpath("//*[@id=\"input-payment-city\"]");
    By postCodeField = By.xpath("//*[@id=\"input-payment-postcode\"]");
    By countryField = By.xpath("//*[@id=\"input-payment-country\"]");
    By regionField = By.xpath("//*[@id=\"input-payment-zone\"]");
    By continueBtnStep2 = By.xpath("//*[@id=\"button-guest\"]");
    By continueBtnStep3 = By.xpath("//*[@id=\"button-guest-shipping\"]");
    By continueBtnStep4 = By.xpath("//*[@id=\"button-shipping-method\"]");
    By conditionsCheckBoxStep5 = By.xpath("//input[@name='agree']");
    By continueBtnStep5 = By.xpath("//*[@id=\"button-payment-method\"]");
    By confirmOrderBtn = By.xpath("//*[@id=\"button-confirm\"]");
    By emailFieldLogin = By.id("input-email");
    By passFieldLogin = By.id("input-password");

    public CheckoutPage(WebDriver driver) {
        super(driver);
    }

    public CheckoutPage verifyCheckoutPageOpened(){
        Assert.assertEquals("Checkout page is not opened", getDriver().getTitle(), "Checkout");
        return this;
    }

    public void goToPage() {
        getDriver().get(getBaseUrl() + "index.php?route=checkout/cart");
    }

    public CheckoutPage checkedVisiblityCheckoutPageTitle(){
        Assert.assertTrue(getElement(checkoutPageTitle).isExists());
        return this;
    }

    public CheckoutPage clickContinueButton(){
        getElement(continueButton).click();
        return this;
    }

    public CheckoutPage checkedVisiblityLabelIsEmpty(String textEmptyLabel){
        BaseElement labelPageEmpty = getElement(checkoutPageEmptyLabel);
        Assert.assertTrue(labelPageEmpty.isExists());
        Assert.assertEquals(labelPageEmpty.getValue(), textEmptyLabel);
        return this;
    }

    public CheckoutPage checkedVisiblityCheckoutOptionsStep1(){
        Assert.assertTrue(getElement(checkoutOptionsStep1).isExists());
        return this;
    }

    public CheckoutPage checkedVisiblitContinueButton(){
        Assert.assertTrue(getElement(continueButton).isExists());
        return this;
    }

    public CheckoutPage clickItemMenuComponent(){
        getElement(itemMenuComponent).click();
        return this;
    }

    public CheckoutPage clickItemSubMenuMonitors(){
        getElement(itemSubMenuMonitors).click();
        return this;
    }

    public CheckoutPage clickGuestCheckoutRadioBtn() {
        getElement(guestCheckoutRadioBtn).click();
        return this;
    }

    public CheckoutPage clickContinueBtnStep1() {
        getElement(continueBtnStep1).click();
        return this;
    }

    public CheckoutPage fillFirstNameField(String s) {
        getElement(firstNameField).sendKeys(s);
        return this;
    }

    public CheckoutPage fillLastNameField(String s) {
        getElement(lastNameField).sendKeys(s);
        return this;
    }

    public CheckoutPage fillEmailField(String s) {
        getElement(emailField).sendKeys(s);
        return this;
    }

    public CheckoutPage fillTelephoneField(String s) {
        getElement(telephoneField).sendKeys(s);
        return this;
    }

    public CheckoutPage fillAddress1Field(String s) {
        getElement(address1Field).sendKeys(s);
        return this;
    }

    public CheckoutPage fillCityField(String s) {
        getElement(cityField).sendKeys(s);
        return this;
    }

    public CheckoutPage fillPostCodeField(String s) {
        getElement(postCodeField).sendKeys(s);
        return this;
    }

    public CheckoutPage fillCountryField(String s) {
        getElement(countryField).sendKeys(s);
        return this;
    }

    public CheckoutPage fillRegionField(String s) {
        getElement(regionField).sendKeys(s);
        return this;
    }

    public CheckoutPage clickContinueBtnStep2() {
        getElement(continueBtnStep2).click();
        return this;
    }

    public CheckoutPage clickContinueBtnStep3() {
        getElement(continueBtnStep3).click();
        return this;
    }

    public CheckoutPage clickContinueBtnStep4() {
        getElement(continueBtnStep4).click();
        return this;
    }

    public CheckoutPage clickConditionsCheckBoxStep5() {
        getElement(conditionsCheckBoxStep5).click();
        return this;
    }

    public CheckoutPage clickContinueBtnStep5() {
        getElement(continueBtnStep5).click();
        return this;
    }

    public CheckoutPage clickConfirmOrderBtn() {
        getElement(confirmOrderBtn).click();
        return this;
    }

    public CheckoutPage enterEmailField(String s){
        getElement(emailFieldLogin).sendKeys(s);
        return this;
    }

    public CheckoutPage enterLoginField(String s){
        getElement(passFieldLogin).sendKeys(s);
        return this;
    }


}
