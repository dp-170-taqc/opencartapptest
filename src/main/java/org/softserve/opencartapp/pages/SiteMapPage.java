package org.softserve.opencartapp.pages;

import org.openqa.selenium.WebDriver;
import org.softserve.opencartapp.core.BasePage;

public class SiteMapPage extends BasePage {

    public SiteMapPage(WebDriver driver) {
        super(driver);
    }

    public void goToPage() {
        getDriver().get(getBaseUrl() + "index.php");
    }


}
