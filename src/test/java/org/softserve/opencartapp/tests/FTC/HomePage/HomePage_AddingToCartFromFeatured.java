package org.softserve.opencartapp.tests.FTC.HomePage;

import org.junit.After;
import org.junit.Test;
import org.openqa.selenium.chrome.ChromeDriver;
import org.softserve.opencartapp.core.BaseTest;
import org.softserve.opencartapp.pages.HomePage;

public class HomePage_AddingToCartFromFeatured extends BaseTest {
    HomePage homePage;

    @Test
    public void addingToCartFromFeaturedMacBook() {
        HomePage homePage = new HomePage(getDriver());
        homePage.goToPage();
        homePage.addToCartMacBookFeatured();
    }

    @Test
    public void addingToCartFromFeaturedIphone() {
        HomePage homePage = new HomePage(getDriver());
        homePage.goToPage();
        homePage.addToCartIphoneFeatured();
    }

    @Test
    public void addingToCartFromFeaturedAppleCinema() {
        HomePage homePage = new HomePage(getDriver());
        homePage.goToPage();
        homePage.addToCartAppleCinemaFeatured();
    }

    @Test
    public void addingToCartFromFeaturedCanonEos() {
        HomePage homePage = new HomePage(getDriver());
        homePage.goToPage();
        homePage.addToCartCanonEosFeatured();
    }
}
