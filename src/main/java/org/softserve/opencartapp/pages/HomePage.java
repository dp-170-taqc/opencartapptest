package org.softserve.opencartapp.pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.softserve.opencartapp.core.BasePage;

public class HomePage extends BasePage {

    By iPhoneSlider = By.xpath("//img[@alt='iPhone 6']");
    By macBookSlider = By.xpath("//img[@alt='MacBookAir']");
    By macBookFeatured = By.cssSelector(".product-layout:nth-child(1) .img-responsive");
    By iPhoneFeatured = By.cssSelector(".product-layout:nth-child(2) .img-responsive");
    By appleCinemaFeatured = By.cssSelector(".product-layout:nth-child(3) .img-responsive");
    By canonEosFeatured = By.cssSelector(".product-layout:nth-child(4) .img-responsive");
    By addToCartBtnMacBook = By.cssSelector(".product-layout:nth-child(1) .fa-shopping-cart");
    By addToCartBtnIphone = By.cssSelector(".product-layout:nth-child(2) .fa-shopping-cart");
    By addToCartBtnAppleCinema = By.cssSelector(".product-layout:nth-child(3) .fa-shopping-cart");
    By addToCartBtnCanonEos = By.cssSelector(".product-layout:nth-child(4) .fa-shopping-cart");
    By successMessageAddToCart = By.xpath("//div[@class='alert alert-success']");

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public void goToPage() {
        getDriver().get(getBaseUrl() + "index.php?route=common/home");
    }

    public HomePage createNewInstanceHomePage() {
        return new HomePage(getDriver());
    }

    public ProductPage goToIphoneSlider() {
        getElement(iPhoneSlider).click();
        Assert.assertEquals("iPhone6", getDriver().getTitle());
        return new ProductPage(getDriver());
    }

    public ProductPage goToMacBookSlider() {
        getElement(macBookSlider).click();
        Assert.assertEquals("MacBook", getDriver().getTitle());
        return new ProductPage(getDriver());
    }

    public ProductPage goToMacBookPageFromFeatured() {
        getElement(macBookFeatured).click();
        Assert.assertEquals("MacBook", getDriver().getTitle());
        return new ProductPage(getDriver());
    }

    public ProductPage goToIphonePageFromFeatured() {
        getElement(iPhoneFeatured).click();
        Assert.assertEquals("iPhone", getDriver().getTitle());
        return new ProductPage(getDriver());
    }

    public ProductPage goToAppleCinemaPageFromFeatured() {
        getElement(appleCinemaFeatured).click();
        Assert.assertEquals("Apple Cinema 30", getDriver().getTitle());
        return new ProductPage(getDriver());
    }

    public ProductPage goToCanonEosPageFromFeatured() {
        getElement(canonEosFeatured).click();
        Assert.assertEquals("Canon EOS 5D", getDriver().getTitle());
        return new ProductPage(getDriver());
    }

    public HomePage addToCartMacBookFeatured() {
        getElement(addToCartBtnMacBook).click();
        Assert.assertTrue(getElement(successMessageAddToCart).isExists());
        return this;
    }

    public HomePage addToCartIphoneFeatured() {
        getElement(addToCartBtnIphone).click();
        Assert.assertTrue(getElement(successMessageAddToCart).isExists());
        return this;
    }

    public HomePage addToCartAppleCinemaFeatured() {
        getElement(addToCartBtnAppleCinema).click();
        Assert.assertTrue(getElement(successMessageAddToCart).isExists());
        return this;
    }

    public HomePage addToCartCanonEosFeatured() {
        getElement(addToCartBtnCanonEos).click();
        Assert.assertTrue(getElement(successMessageAddToCart).isExists());
        return this;
    }


    public HomePage verifyCurrentUrl(String url) {
        Assert.assertEquals(getDriver().getCurrentUrl(), url);
        return this;
    }


}

