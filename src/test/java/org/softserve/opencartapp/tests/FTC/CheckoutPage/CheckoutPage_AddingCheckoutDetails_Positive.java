package org.softserve.opencartapp.tests.FTC.CheckoutPage;

import org.junit.Test;
import org.softserve.opencartapp.core.BaseTest;
import org.softserve.opencartapp.pages.CheckoutPage;

public class CheckoutPage_AddingCheckoutDetails_Positive extends BaseTest {
    CheckoutPage page;

    @Test
    public void addProductToCheckout(){
        page = new CheckoutPage(getDriver());
        page.goToPage();
        page.checkedVisiblityCheckoutPageTitle()
                .clickItemMenuComponent()
                .clickItemSubMenuMonitors()
                .checkedVisiblityCheckoutOptionsStep1();
    }
}
