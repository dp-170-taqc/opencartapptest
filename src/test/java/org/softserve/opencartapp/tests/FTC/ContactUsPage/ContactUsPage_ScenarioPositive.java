package org.softserve.opencartapp.tests.FTC.ContactUsPage;

import org.junit.Test;
import org.softserve.opencartapp.core.BaseTest;
import org.softserve.opencartapp.pages.ContactUsPage;
import org.softserve.opencartapp.utilities.ConstTextValues;

public class ContactUsPage_ScenarioPositive extends BaseTest {

    ContactUsPage page;

    @Test
    public void test(){
        page = new ContactUsPage(getDriver());
        page.goToPage();
        page.fillYourNameFormName(ConstTextValues.name)
                .fillEmailFormEmail(ConstTextValues.eMail)
                .fillEnquiry(ConstTextValues.test17chars)
                .clickSubmitButtonAndMoveToSuccessPage()
                .verifySuccessMsg(ConstTextValues.enquirySuccessMsg)
                .clickContinueButtonAndMoveToHomePage()
                .verifyCurrentUrl(ConstTextValues.currentHomePageUrl);
    }

}
