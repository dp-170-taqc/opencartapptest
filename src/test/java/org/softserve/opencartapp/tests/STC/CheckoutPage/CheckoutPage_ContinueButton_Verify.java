package org.softserve.opencartapp.tests.STC.CheckoutPage;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.softserve.opencartapp.core.BaseElement;
import org.softserve.opencartapp.core.BaseTest;
import org.softserve.opencartapp.pages.CheckoutPage;

public class CheckoutPage_ContinueButton_Verify extends BaseTest {

    private CheckoutPage page;

    @Test
    public void STC_CheckoutPage_ClickContinueButton () {
        page = new CheckoutPage(getDriver());
        page.goToPage();
        page.checkedVisiblitContinueButton()
                .clickContinueButton();
    }

}