package org.softserve.opencartapp.tests.FTC.LoginPage;

import org.junit.Test;
import org.softserve.opencartapp.core.BaseTest;
import org.softserve.opencartapp.pages.LoginPage;
import org.softserve.opencartapp.utilities.ConstTextValues;

public class LoginPage_LoginScenarioNegative extends BaseTest {
    LoginPage page;

    @Test
    public void test(){
        page = new LoginPage(getDriver());
        page.goToPage();
        page.clickLoginButton()
                .verifyLoginErrorMsgShownAndCorrect(ConstTextValues.loginErrorMsg)
                .fillEmailOnLoginPage(ConstTextValues.eMail)
                .fillPasswordOnLoginPage(ConstTextValues.password1to5)
                .clickLoginButton()
                .verifyLoginErrorMsgShownAndCorrect(ConstTextValues.loginErrorMsg)
                .clearPasswordLoginForm()
                .clickLoginButton()
                .verifyLoginErrorMsgShownAndCorrect(ConstTextValues.loginErrorMsg)
                .clearEMailLoginForm()
                .fillPasswordOnLoginPage(ConstTextValues.password1to5)
                .verifyLoginErrorMsgShownAndCorrect(ConstTextValues.loginErrorMsg);

    }

}
