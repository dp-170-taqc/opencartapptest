package org.softserve.opencartapp.tests.STC.SuccessPage;

import org.junit.Test;
import org.softserve.opencartapp.core.BaseTest;
import org.softserve.opencartapp.pages.LoginPage;
import org.softserve.opencartapp.pages.SuccessPage;
import org.softserve.opencartapp.utilities.ConstTextValues;

public class SuccessPage_WholePageVisibility extends BaseTest {

    SuccessPage page;

    @Test
    public void test(){
        page = new SuccessPage(getDriver());
        page.goToPage();
        page.checkedContimueButton()
                .checkedSuccessTitle()
                .verifySuccessMsg(ConstTextValues.enquirySuccessMsg);
    }
}
