package org.softserve.opencartapp.core;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.softserve.opencartapp.helplers.Footer;
import org.softserve.opencartapp.helplers.Header;

import java.util.concurrent.TimeUnit;

public abstract class BasePage {
    private WebDriver driver;
    private final String BASE_URL = "http://taqc-dp170.zzz.com.ua/";
    private Header header;
    private Footer footer;

    public BasePage (WebDriver driver){
        driver.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        this.driver = driver;
    }

    public WebDriver getDriver() {
        return driver;
    }

    public void setDriver(WebDriver driver) {
        this.driver = driver;
    }
    public BaseElement getElement (By elementBy) {
        BaseElement baseElement = null;
        try {
            baseElement = new BaseElement(driver.findElement(elementBy));
        }catch (NoSuchElementException e){
            baseElement = new BaseElement(null);
        }
        return baseElement;
    }

    public void wait(int timeout){
        try{
            Thread.sleep(timeout);
        }catch(Exception e){
        }
    }

    public abstract void goToPage();

    public String getBaseUrl() { return BASE_URL;};

}
