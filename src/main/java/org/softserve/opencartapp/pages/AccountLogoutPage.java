package org.softserve.opencartapp.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.softserve.opencartapp.core.BaseElement;
import org.softserve.opencartapp.core.BasePage;

public class AccountLogoutPage extends BasePage {

    By loginButton = By.xpath("//*[@id=\"column-right\"]/div/a[1]");

    public AccountLogoutPage(WebDriver driver) {
        super(driver);
    }

    public void goToPage() {
        getDriver().get(getBaseUrl() + "index.php?route=account/logout");
    }

    public LoginPage clickLoginButtonFromRightBlock() {
        getElement(loginButton).click();
        return new LoginPage(getDriver());
    }
}
