package org.softserve.opencartapp.tests.FTC.ProductPage;

import org.junit.Assert;
import org.junit.Test;
import org.softserve.opencartapp.core.BaseTest;
import org.softserve.opencartapp.pages.HomePage;
import org.softserve.opencartapp.pages.ProductPage;

public class ProductPage_WritingAReviewPositive extends BaseTest {

    @Test
    public void writingAReviewTestPositive() {
        HomePage homePage = new HomePage(getDriver());
        homePage.goToPage();
        homePage.goToIphonePageFromFeatured()
                .openReviewLink()
                .enterYourName("hklhlkj")
                .enterYourReview("gj jgkg klgjg erjklgrklgjkgjgkljgl elgj")
                .selectRatingRadioBtn()
                .clickContinueBtn()
                .checkedSuccessMsg();
    }
}
