package org.softserve.opencartapp.pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.softserve.opencartapp.core.BasePage;

public class ProductPage extends BasePage {

    By productPrice = By.xpath("//h2[contains(.,'$602.00')]");
    By productName = By.xpath("//h1[contains(.,'MacBook')]");
    By macBookPicture1 = By.xpath("(//img[@alt='MacBook'])[2]");
    By macBookPictureOpened = By.cssSelector(".mfp-img");
    By compareBtn = By.cssSelector(".fa-exchange");
    By quantityField = By.id("input-quantity");
    By addToCartButton = By.id("button-cart");
    By writeAReviewLink = By.xpath("//a[contains(.,'Write a review')]");
    By yourNameField = By.id("input-name");
    By yourReviewField = By.id("input-review");
    By continueBtn = By.id("button-review");
    By successMessage = By.xpath("//div[@class='alert alert-success' and contains(text(), 'Success: You have added ')]");
    By errorMessage = By.xpath("//div[@class=\"alert alert-danger\"]");
    By ratingOne = By.xpath("//input[@name='rating']");
    By shoppingCartLink = By.xpath("//a[contains(text(),'shopping cart')]");

    public ProductPage(WebDriver driver) {
        super(driver);
    }

    public void goToPage() {
        getDriver().get(getBaseUrl() + "index.php?route=common/home");
    }

    public ProductPage createNewInstanceProductPage() {
        return new ProductPage(getDriver());
    }

    public ProductPage clearQuantityField() {
        getElement(quantityField).clear();
        return this;
    }

    public ProductPage setQuantity(String s) {
        getElement(quantityField).sendKeys(s);
        return this;
    }

    public ProductPage clickBtnAddToCart() {
        getElement(addToCartButton).click();
        //Assert.assertTrue(getElement(successMessage).isExists());
        return new ProductPage(getDriver());
    }

    public ProductPage clickBtnAddToCompare() {
        getElement(compareBtn).click();
        return this;
    }

    public ProductPage openProductPhoto() {
        getElement(macBookPicture1).click();
        Assert.assertTrue(getElement(macBookPictureOpened).isExists());
        return this;
    }

    public ProductPage openReviewLink() {
        getElement(writeAReviewLink).click();
        Assert.assertTrue(getElement(yourNameField).isExists());
        Assert.assertTrue(getElement(yourReviewField).isExists());
        Assert.assertTrue(getElement(continueBtn).isExists());
        return new ProductPage(getDriver());
    }

    public ProductPage enterYourName(String s) {
        getElement(yourNameField).sendKeys(s);
        return this;
    }

    public ProductPage enterYourReview(String s) {
        getElement(yourReviewField).sendKeys(s);
        return this;
    }

    public ProductPage selectRatingRadioBtn() {
        getElement(ratingOne).click();
        return this;
    }

    public ProductPage clickContinueBtn() {
        getElement(continueBtn).click();
        return new ProductPage(getDriver());
    }

    public ProductPage checkedSuccessMsg() {
        Assert.assertTrue(getElement(successMessage).isExists());
        return this;
    }

    public ProductPage checkedErrorMsg() {
        Assert.assertTrue(getElement(errorMessage).isExists());
        return this;
    }

    public ShoppingCartListPage clickShoppingCartLink() {
        getElement(shoppingCartLink).click();
        return new ShoppingCartListPage(getDriver());
    }
}
