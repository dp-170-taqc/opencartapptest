package org.softserve.opencartapp.helplers;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.softserve.opencartapp.core.BasePage;
import org.softserve.opencartapp.pages.ProductPage;
import org.softserve.opencartapp.pages.SearchPage;
import org.softserve.opencartapp.pages.ShoppingCartListPage;

public class Header extends BasePage {

    By mainMenuTopBar = By.xpath("//*[@id='top']");
    By mainMenuNavBar = By.xpath("//div[@class = 'navbar-header']");
    By mainMenuBreadCrumb = By.xpath("//*[@class = 'breadcrumb']");
    By searchField = By.xpath("//*[@class='form-control input-lg']");
    By searchBtn = By.xpath("//*[@class=\"input-group-btn\"]");
    By shoppingButton = By.id("cart");
    By viewShoppingCart = By.xpath("//*[@class='text-right']/a//i[@class='fa fa-shopping-cart']");

    public Header(WebDriver driver) {
        super(driver);
    }

    public void goToPage() {
        getDriver().get(getBaseUrl() + "index.php");
    }

    public Header checkedVisiblityMainMenuTopBar() {
        Assert.assertTrue(getElement(mainMenuTopBar).isExists());
        return this;
    }

    public Header checkedVisiblityMainMenuNavBar() {
        Assert.assertTrue(getElement(mainMenuNavBar).isExists());
        return this;
    }

    public Header checkedVisiblityMainMenuBreadCrumb() {
        Assert.assertTrue(getElement(mainMenuBreadCrumb).isExists());
        return this;
    }

    public Header enterSearchField(String s){
        getElement(searchField).sendKeys(s);
        return this;
    }

    public SearchPage clickSearch(){
        getElement(searchBtn).click();
        return new SearchPage(getDriver());
    }

    public Header clickShopppingBtn(){
        wait(2000);
        getElement(shoppingButton).click();
        return this;
    }

    public ShoppingCartListPage clickViewShopping(){
        wait(2000);
        getElement(viewShoppingCart).click();
        return new ShoppingCartListPage(getDriver());
    }

}

