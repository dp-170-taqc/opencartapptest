package org.softserve.opencartapp.pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.softserve.opencartapp.core.BasePage;

public class SuccessPage extends BasePage {

    By successMsg = By.xpath("//*[@id=\"content\"]/p");
    By continueButton = By.xpath("//*[@id=\"content\"]/div/div/a");
    By successTitle = By.xpath("//*[@id=\"content\"]/h1");

    public SuccessPage(WebDriver driver) {
        super(driver);
    }


    public void goToPage() {
        getDriver().get(getBaseUrl() + "index.php?route=information/contact/success");
    }


    public SuccessPage checkedContimueButton() {
        Assert.assertTrue(getElement(continueButton).isExists());
        return this;
    }

    public SuccessPage checkedSuccessTitle() {
        Assert.assertTrue(getElement(successTitle).isExists());
        return this;
    }

    public SuccessPage verifySuccessMsg(String message) {
        Assert.assertEquals(getElement(successMsg).getValue(), message);
        return this;
    }

    public HomePage clickContinueButtonAndMoveToHomePage() {
        getElement(continueButton).click();
        return new HomePage(getDriver());
    }

}
