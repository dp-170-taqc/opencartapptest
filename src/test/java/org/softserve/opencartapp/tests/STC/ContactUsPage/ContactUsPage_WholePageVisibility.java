package org.softserve.opencartapp.tests.STC.ContactUsPage;

import org.junit.Test;
import org.softserve.opencartapp.core.BaseTest;
import org.softserve.opencartapp.pages.ContactUsPage;


public class ContactUsPage_WholePageVisibility extends BaseTest {
    ContactUsPage page;

    @Test
    public void test(){
        page = new ContactUsPage(getDriver());
        page.goToPage();
        page.checkedVisiblityContactUsTitle()
                .checkedVisiblitourLocationInfoTitle()
                .checkedVisiblitOurLocationInfoInformation()
                .checkedVisiblitContactFormBlockTitle()
                .checkedVisiblitYourNameInputForm()
                .checkedVisiblitEMailAdressForm()
                .checkedVisiblityEnquryForm()
                .checkedVisiblitSubmitButton();
    }
}