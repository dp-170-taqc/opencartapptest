package org.softserve.opencartapp.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.softserve.opencartapp.core.BasePage;

public class AffiliatePage extends BasePage {

    By continueBtn = By.linkText("Continue");
    By emailField = By.name("email");
    By password = By.name("password");
    By loginBtn = By.name("redirect");

    public AffiliatePage(WebDriver driver) {
        super(driver);
    }

    public void goToPage() {
        getDriver().get(getBaseUrl()+ "index.php?route=affiliate/login");
    }

    public AffiliateRegisterPage clickContinueBtn() {
        getElement(continueBtn).click();
        return new AffiliateRegisterPage(getDriver());
    }

    public AffiliatePage enterEmail(String s) {
        getElement(emailField).sendKeys(s);
        return this;
    }

    public AffiliatePage enterPassword(String s) {
        getElement(password).sendKeys(s);
        return this;
    }

    public AffiliatePage clickLogin() {
        getElement(loginBtn).click();
        return this;
    }
}
