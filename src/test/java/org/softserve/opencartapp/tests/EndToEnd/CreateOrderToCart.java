package org.softserve.opencartapp.tests.EndToEnd;

import org.junit.Test;
import org.softserve.opencartapp.core.BaseTest;
import org.softserve.opencartapp.pages.CheckoutPage;
import org.softserve.opencartapp.pages.HomePage;
import org.softserve.opencartapp.pages.ProductPage;
import org.softserve.opencartapp.pages.ShoppingCartListPage;

public class CreateOrderToCart extends BaseTest {
    HomePage homePage;
    ProductPage productPage;
    ShoppingCartListPage shopPage;
    CheckoutPage checkoutPage;

    @Test
    public void test() {
        homePage = new HomePage(getDriver());
        homePage.goToPage();
        homePage.goToMacBookPageFromFeatured();
        productPage = new ProductPage(getDriver());
        productPage.clearQuantityField()
                .setQuantity("1")
                .clickBtnAddToCart()
                .checkedSuccessMsg()
                .clickShoppingCartLink();
        shopPage = new ShoppingCartListPage(getDriver());
        shopPage.clearQuantityField()
                .setQuantity("2")
                .clickRefreshForQuantity()
                .clickContinueShoppingBtn();
        homePage.goToIphonePageFromFeatured();
        productPage.clearQuantityField()
                .setQuantity("1")
                .clickBtnAddToCart()
                .checkedSuccessMsg()
                .clickShoppingCartLink();
        shopPage.clickCheckout();
        checkoutPage = new CheckoutPage(getDriver());
        checkoutPage.clickGuestCheckoutRadioBtn()
                .clickContinueBtnStep1()
                .fillFirstNameField("Ivan")
                .fillLastNameField("Ivanov")
                .fillEmailField("dhfjkds@gmail.com")
                .fillTelephoneField("123456")
                .fillAddress1Field("Pobeda street")
                .fillCityField("Dnipro")
                .fillPostCodeField("49000")
                .fillCountryField("Ukraine")
                .fillRegionField("Dnipropetrovs'ka Oblast'")
                .clickContinueBtnStep2()
                .clickContinueBtnStep3()
                .clickContinueBtnStep4()
                .clickConditionsCheckBoxStep5()
                .clickContinueBtnStep5()
                .clickConfirmOrderBtn();
    }
}
