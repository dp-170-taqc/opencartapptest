package org.softserve.opencartapp.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.softserve.opencartapp.core.BasePage;

public class AccountChangePasswordPage extends BasePage {

    By formToFillPassword = By.xpath("//*[@id=\"input-password\"]");
    By formToFillConfirmPassword = By.xpath("//*[@id=\"input-confirm\"]");
    By continueButton = By.xpath("//*[@id=\"content\"]/form/div/div[2]/input");

    public AccountChangePasswordPage(WebDriver driver) {
        super(driver);
    }

    public void goToPage() {
        getDriver().get(getBaseUrl() + "index.php?route=account/password");
    }

    public AccountChangePasswordPage fillFormToFillPassword(String pass) {
        getElement(formToFillPassword).sendKeys(pass);
        return this;
    }

    public AccountChangePasswordPage fillFormToFillConfirmPassword(String pass) {
        getElement(formToFillConfirmPassword).sendKeys(pass);
        return this;
    }

    public AccountPage clickContinueButton(){
        getElement(continueButton).click();
        return new AccountPage(getDriver());
    }

}
