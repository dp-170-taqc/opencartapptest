package org.softserve.opencartapp.tests.FTC.AffiliatePage;

import org.junit.Assert;
import org.junit.Test;
import org.softserve.opencartapp.core.BaseTest;
import org.softserve.opencartapp.pages.AffiliateRegisterPage;
import org.softserve.opencartapp.pages.AffiliatePage;

public class AffiliatePage_RegistrationUser extends BaseTest {

    @Test
    public void newUserRegisterScenarioPositive() {
        AffiliateRegisterPage affiliateRegisterPage = new AffiliateRegisterPage(getDriver());
        AffiliatePage affiliatePage = new AffiliatePage(getDriver());

        affiliatePage.goToPage();
        affiliatePage.clickContinueBtn();

        affiliateRegisterPage.enterFirstName("Petr")
                .enterLastName("Ivanov")
                .enterEmail("ivanov@mail.ru")
                .enterTelephone("0678453215")
                .enterAddress1("Street 1")
                .enterCity("Dnipro")
                .enterPostCode("42210")
                .enterCountry("Ukraine")
                .enterRegion("Dnipropetrovs'ka Oblast'")
                .enterPassword("qwerty")
                .enterPasswordConfirm("qwerty")
                .clickCheckBtn();
        affiliateRegisterPage.clickContinueBtn()
                         .clickLogout();

        affiliatePage.goToPage();
        affiliatePage.enterEmail("ivanov@mail.ru")
                .enterPassword("qwerty")
                .clickLogin();
        Assert.assertTrue(getDriver().getTitle().equals("My Affiliate Account"));


    }
}
