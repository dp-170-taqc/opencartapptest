package org.softserve.opencartapp.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.softserve.opencartapp.core.BasePage;

public class AffiliateRegisterPage extends BasePage {

    By firstNameField = By.name("firstname");
    By lastNameField = By.name("lastname");
    By emailField = By.name("email");
    By telephoneField = By.name("telephone");
    By address_1Field = By.name("address_1");
    By cityField = By.name("city");
    By postcodeField = By.name("postcode");
    By inputCountryField = By.id("input-country");
    By inputRegionField = (By.id("input-zone"));
    By passwordField = (By.name("password"));
    By passwordConfirmField = By.name("confirm");
    By continueBtn = By.xpath("//*[@id=\"content\"]/form/div/div/input[2]");
    By agreeCheckBox = By.name("agree");
    By logoutLink = By.xpath("//*[@id=\"column-right\"]/div/a[7]");

    SuccessPage successPage = new SuccessPage(getDriver());

    public AffiliateRegisterPage(WebDriver driver) {
        super(driver);
    }

    public void goToPage() {
        getDriver().get(getBaseUrl() + "index.php?route=affiliate/register");
    }


    public AffiliateRegisterPage enterFirstName(String s) {
        getElement(firstNameField).sendKeys(s);
        return this;
    }

    public AffiliateRegisterPage enterLastName(String s) {
        getElement(lastNameField).sendKeys(s);
        return this;
    }

    public AffiliateRegisterPage enterEmail(String s) {
        getElement(emailField).sendKeys(s);
        return this;
    }

    public AffiliateRegisterPage enterTelephone(String s) {
        getElement(telephoneField).sendKeys(s);
        return this;
    }

    public AffiliateRegisterPage enterAddress1(String s) {
        getElement(address_1Field).sendKeys(s);
        return this;
    }

    public AffiliateRegisterPage enterCity(String s) {
        getElement(cityField).sendKeys(s);
        return this;
    }

    public AffiliateRegisterPage enterPostCode(String s) {
        getElement(postcodeField).sendKeys(s);
        return this;
    }

    public AffiliateRegisterPage enterCountry(String s) {
        getElement(inputCountryField).sendKeys(s);
        return this;
    }

    public AffiliateRegisterPage enterRegion(String s) {
        getElement(inputRegionField).sendKeys(s);
        return this;
    }

    public AffiliateRegisterPage enterPassword(String s) {
        getElement(passwordField).sendKeys(s);
        return this;
    }

    public AffiliateRegisterPage enterPasswordConfirm(String s) {
        getElement(passwordConfirmField).sendKeys(s);
        return this;
    }

    public AffiliateRegisterPage clickContinueBtn() {
        getElement(continueBtn).click();
        return this;
    }

    public SuccessPage clickCheckBtn() {
        getElement(agreeCheckBox).click();
        return this.successPage;
    }

    public AffiliateRegisterPage clickLogout() {
        getElement(logoutLink).click();
        return this;
    }

}
