package org.softserve.opencartapp.tests.EndToEnd;

import org.junit.Test;
import org.softserve.opencartapp.core.BaseTest;
import org.softserve.opencartapp.helplers.Header;
import org.softserve.opencartapp.pages.HomePage;
import org.softserve.opencartapp.pages.ShoppingCartListPage;

public class ETE_AddingProductToShoppingCart extends BaseTest {
    HomePage homePage;

    @Test
    public void addToShoppingCart() {
        ShoppingCartListPage shPage = new ShoppingCartListPage(getDriver());
        Header header = new Header(getDriver());
        homePage = new HomePage(getDriver());

        homePage.goToPage();
        header.enterSearchField("MacBook")
                .clickSearch()
                .clickAddBtn();
        header.clickShopppingBtn()
                .clickViewShopping();
        shPage.clearQuantityField()
                .setQuantity("2")
                .clickRefreshForQuantity()
                .clickUseCouponCode()
                .setCouponCode("19-19-19")
                .clickApplyCouponBtn()
                .clickUseGiftCertificate()
                .setGiftCertificate("18-18-18")
                .clickApplyGiftCertificate()
                .clickCheckout()
                .verifyCheckoutPageOpened();


    }
/*
    @Test
    public void addItem() {
        ShoppingCartListPage shPage = new ShoppingCartListPage(getDriver());
        HomePage homePage = new HomePage(getDriver());
        homePage.goToPage();
        shPage.clickAddToShoppingCart();
        Assert.assertTrue(shPage.getElement(shPage.getEmptyShoppingCart()).getValue().equals("Your shopping cart is empty!"));
    }
*/

}
