package org.softserve.opencartapp.pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.softserve.opencartapp.core.BasePage;

public class AccountPage extends BasePage {

    By editYourAccInfo = By.xpath("//*[@id=\"content\"]/ul[1]/li[1]/a");
    By changeYourPassword = By.xpath("//*[@id=\"content\"]/ul[1]/li[2]/a");
    By logoutButtonRightBlock = By.xpath("//*[@id=\"column-right\"]/div/a[13]");

    public AccountPage(WebDriver driver) {
        super(driver);
    }

    public void goToPage() {
        getDriver().get(getBaseUrl() + "index.php?route=account/account");
    }

    public AccountPage checkedEditYourAccInfo(){
        Assert.assertTrue(getElement(editYourAccInfo).isExists());
        return this;
    }

    public AccountPage verifyCurrentUrl(String url){
        Assert.assertEquals(getDriver().getCurrentUrl(), url);
        return this;
    }

    public AccountChangePasswordPage clickChangeYourPasswordAndMoveToAccountChangePasswordPage(){
        getElement(changeYourPassword).click();
        return new AccountChangePasswordPage(getDriver());
    }

    public AccountLogoutPage clickLogout(){

        getElement(logoutButtonRightBlock).click();
        return new AccountLogoutPage(getDriver());
    }




}
