package org.softserve.opencartapp.tests.FTC.ProductPage;

import org.junit.Test;
import org.softserve.opencartapp.core.BaseTest;
import org.softserve.opencartapp.pages.HomePage;
import org.softserve.opencartapp.pages.ProductPage;

public class ProductPage_AddingToCartNegative extends BaseTest {
    ProductPage productPage;

    @Test
    public void addingToCartTest() {
        HomePage homePage = new HomePage(getDriver());
        homePage.goToPage();
        homePage.goToIphonePageFromFeatured()
                .clearQuantityField()
                .clickBtnAddToCart()
                .checkedErrorMsg();
    }
}
